import React from 'react';
import { Link } from 'react-router-dom';

import logoImg from '../../assets/images/logo-grupo-mateus-colorido.png';

import './styles.css';

interface PageHeaderProps {
    title: string;
  }

const PageHeader: React.FC<PageHeaderProps> = ({ title }) => {
    return (
        <header className="page-header">
            <div className="top-bar-container">
                <Link className="logout" to="/">
                    Sair
                </Link>
                <img src={logoImg} alt="Logo mateus colorido" />
            </div>

            <div className="header-content">
                <strong>{title}</strong>
            </div>
        </header>
    );
}

export default PageHeader;