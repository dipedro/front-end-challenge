import React, { FormEvent, useEffect, useState } from "react";
import io from "socket.io-client";
import "./styles.css";

interface messageObject {
  author: string;
  message: string;
  subject: string;
  userId: string;
}

interface ChatProps {
  name: string;
  label: string;
  subject: string;
  ticket: string;
}

const userId = localStorage.getItem("userId");
const id = localStorage.getItem("id");
const clientName = localStorage.getItem("name");
let socket: SocketIOClient.Socket;

const Chat: React.FC<ChatProps> = ({ label, ticket }) => {
  const [message, setMessage] = useState("");
  const [mensagensTicket, setMensagensTicket] = useState(
    new Array<messageObject>()
  );

  const options = {
    timeZone:"America/Sao_Paulo",
    hour12 : true,
    hour:  "2-digit",
    minute: "2-digit"
 };

  useEffect(() => {
    socket = io('http://localhost:3001');
  }, []);

  useEffect(() => {

    if (!socket) return;
 
    socket.on('connect', () => {
      console.log('socket connect in useEffect with ID: ' + socket.id);
      socket.emit('registerClient', {
        client_id: id, 
        ticket_id: ticket, 
        socket_id: socket.id,
        user_id: userId
      })
    });

    socket.on('disconnect', () => {
      console.log('socket disconnect');
    });

    socket.on("historyMessages", (messages_list: any) => {
      console.log("historyMessages");
      setMensagensTicket(messages_list ? messages_list : []);
    });

    socket.on("receivedMessage", (messages_list: messageObject) => {
      console.log("receivedMessage");
      setMensagensTicket((mensagensTicket) => [
        ...mensagensTicket,
        messages_list,
      ]);
    });

    return () => {
      socket.disconnect();
    }
  }, [ticket]);

  function handleMessage(e: FormEvent) {
    e.preventDefault();

    const messageObject = {
      message: message,
      ticket: ticket,
      userId: userId,
    };
    socket.emit("sendMessage", messageObject);
    setMessage("");
  }

  return (
    <>
      <section className="msger">
        <header className="msger-header">
          <div className="msger-header-title">
            <i className="fas fa-comment-alt"></i> Atendimento - SAC Mateus
          </div>
          <div className="msger-header-options">
            <span>
              {label}
            </span>
          </div>
        </header>
        <main className="msger-chat">
          <div className="msg left-msg">
            <div className="msg-bubble">
              <div className="msg-info">
                <div className="msg-info-name">Atendente</div>
                <div className="msg-info-time">{new Date().toLocaleTimeString("pt-BR",options)}</div>
              </div>

              <div className="msg-text">
                Olá, bem vindo(a) ao SAC Mateus! Digite sua mensagem abaixo...
              </div>
            </div>
          </div>

          {(mensagensTicket || []).map((message) => {
            return (
              <div key={message.message} className="msg right-msg">
                <div className="msg-bubble">
                  <div className="msg-info">
                    <div className="msg-info-name">{clientName}</div>
                    <div className="msg-info-time">{new Date().toLocaleTimeString("pt-BR",options)}</div>
                  </div>

                  <div className="msg-text">{message.message}</div>
                </div>
              </div>
            );
          })}
        </main>

        <form onSubmit={handleMessage} className="msger-inputarea">
          <input
            type="text"
            className="msger-input"
            placeholder="Digite sua mensagem..."
            value={message}
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
          <button type="submit" className="msger-send-btn">
            Enviar
          </button>
        </form>
      </section>
    </>
  );
};

export default Chat;
