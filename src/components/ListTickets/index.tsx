import React, { useEffect, useState } from "react";
import PermPhoneMsgIcon from "@material-ui/icons/PermPhoneMsg";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import SendIcon from "@material-ui/icons/Send";

import "./styles.css";
import ListItemText from "@material-ui/core/ListItemText";
import api from "../../services/api";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";

const ListTickets = () => {
  const [total, setTotal] = useState(0);
  const [tickets, setTickets] = useState([]);
  const [detailItem, setDetailItem] = useState({});

  useEffect(() => {
    api.get("tickets").then((response) => {
      setTickets(response.data);
      setTotal(response.data.length);
    });
  }, []);

  function getDetail(ticket: any) {
    setDetailItem(ticket);
    console.log(detailItem);
  }

  return (
    <div id="page">
      <div className="conteudo">
        <div className="header-container">
          <div className="header-talk">
            <PermPhoneMsgIcon style={{ fontSize: 36 }} />
            <h2> Conversas</h2>
          </div>
          <hr />
          <p>{total} Atendimento(s) Pendente(s)</p>
        </div>
        <div className="list-tickets">
          {tickets.map((ticket: any) => {
              return(
              <List key={ticket._id} component="nav" aria-label="list tickets">
              <ListItem button onClick={() => getDetail(ticket)}>
                <ListItemIcon>
                  <SendIcon style={{ fontSize: 36 }} />
                </ListItemIcon>
                <ListItemText primary={`${ticket.client.name} / ${ticket.client.state} / ${ticket.subject.name}`} />
                <ListItemSecondaryAction>
                {ticket.status}
                  </ListItemSecondaryAction>
              </ListItem>
            </List>
            )
          })}
        </div>
      </div>
    </div>
  );
};

export default ListTickets;
