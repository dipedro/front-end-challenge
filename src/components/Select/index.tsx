import React, { SelectHTMLAttributes } from "react";

import './styles.css';

interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  name: string;
  label: string;
  options: Array<{
    _id: string;
    name: string;
  }>;
}

const Select: React.FC<SelectProps> = ({ label, name, options, ...rest }) => {
    return (
        <div className="select-block">
          <label htmlFor={name}>{label}</label>
          <select id={name} {...rest}>
            <option value="" disabled hidden>Selecione uma opção</option>
            {options.map(option => {
              return <option key={option._id} value={option._id}>{option.name}</option>
            })}
          </select>
        </div>
    );
}

export default Select;