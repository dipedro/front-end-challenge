import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Login from './pages/Login';
import Register from './pages/Register';
import Client from './pages/Client';
import Attendant from './pages/Attendant';
import Faq from './pages/Faq';

function Routes() {
    return (
        <BrowserRouter>
            <Route path="/" exact component={Login}/>
            <Route path="/cadastrar" component={Register}/>
            <Route path="/cliente" component={Client}/>
            <Route path="/faq" component={Faq}/>
            <Route path="/atendente" component={Attendant}/>
        </BrowserRouter>
    )
}

export default Routes;
