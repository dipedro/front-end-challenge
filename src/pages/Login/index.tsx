import React, { FormEvent, useState } from "react";
import { useHistory } from 'react-router-dom';
import { Link } from "react-router-dom";
import Input from "../../components/Input";
import logoImg from "../../assets/images/logo-grupo-mateus-colorido.png";

import "./styles.css";
import api from "../../services/api";
import { cpfMask } from "../../utils/cpfMask";

function Login() {
  const history = useHistory();

  const [cpf, setCpf] = useState('');
  const [password, setPassword] = useState('');

  function handleAuth(e: FormEvent) {
    e.preventDefault();

    api
      .post('auth', { cpf, password })
      .then((response) => {
        localStorage.setItem('name', response.data.userType === 'client' ? 
            response.data.client.name: response.data.attendant.name);
        localStorage.setItem('userId', response.data.userId);
        localStorage.setItem('id', response.data.userType === 'client' ? 
          response.data.client._id : response.data.attendant._id);
        localStorage.setItem('userType', response.data.userType);
        response.data.userType === 'client' ? history.push('/cliente') : history.push('/atendente');
      })
      .catch(() => {
        alert("Login ou senha incorretos.");
      });
  }

  return (
    <div id="page-login">
      <div id="page-login-content" className="container">
        <div className="logo-container">
          <img src={logoImg} alt="Logo Grupo Mateus" />
          <h2>Serviço de Atendimento ao Consumidor - SAC Mateus</h2>
        </div>
        <div className="middle">
          <form onSubmit={handleAuth} id="register-form">
            <Input
              name="cpf"
              type="text"
              label="CPF"
              value={cpf}
              onChange={(e) => {
                setCpf(cpfMask(e.target.value));
              }}
            />
            <Input
              name="password"
              type="password"
              label="Senha"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />

            <div className="buttons-container">
              <button type="submit" className="btn-login">
                Entrar
              </button>

              <Link to="/cadastrar" className="btn-register">
                Cadastre-se
              </Link>
            </div>
          </form>
        </div>

        <span className="footer">Feito por Pedro Azevedo.</span>
      </div>
    </div>
  );
}

export default Login;
