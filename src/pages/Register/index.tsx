import React, { FormEvent, useState } from "react";
import { useHistory } from "react-router-dom";
import Input from "../../components/Input";
import PageHeader from "../../components/PageHeader";
import api from "../../services/api";
import { cpfMask } from "../../utils/cpfMask";
import "./styles.css";

function Register() {
  const history = useHistory();

  const [name, setName] = useState('');
  const [cpf, setCpf] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [state, setState] = useState('');
  const [password, setPassword] = useState('');

  function handleRegister (e: FormEvent) {
    e.preventDefault();

    api
      .post("clients", { name, cpf, email, phone, state, password })
      .then((response) => {
        console.log(response);
        alert('Cliente cadastrado com sucesso!');
        history.push('/');
      })
      .catch(() => {
        alert("Erro no cadastro!");
      });

  }

  return (
    <div id="page-register" className="container">
      <PageHeader title="Cadastre suas informações para iniciar o atendimento:" />
      <form onSubmit={handleRegister} id="register-form">

        <Input 
          name="name" 
          type="text" 
          label="Nome"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }} />

        <Input 
          name="cpf" 
          type="text" 
          label="CPF"
          value={cpf}
          onChange={(e) => {
            setCpf(cpfMask(e.target.value));
          }} />

        <Input 
          name="email" 
          type="email" 
          label="E-mail"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }} />

        <Input 
          name="phone" 
          type="text" 
          label="Telefone"
          value={phone}
          onChange={(e) => {
            setPhone(e.target.value);
          }} />

        <Input 
          name="state" 
          type="text" 
          label="Estado"
          value={state}
          onChange={(e) => {
            setState(e.target.value);
          }} />

        <Input 
          name="password" 
          type="password" 
          label="Senha"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }} />

        <div className="button-container">

          <button type="submit" className="btn-register">
            Cadastrar
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;
