import React, { useState, useEffect, FormEvent } from "react";
import PageHeader from "../../components/PageHeader";
import Select from "../../components/Select";
import api from "../../services/api";
import "./styles.css";
import { Link } from "react-router-dom";
import Chat from "../../components/Chat";



function Client() {
  const clientName = localStorage.getItem('name');
  const id = localStorage.getItem('id');
  const [subjects, setSubjects] = useState([]);
  const [subject, setSubject] = useState('');
  const [subjectName, setSubjectName] = useState('');
  const [ticket, setTicket] = useState('');

  useEffect(() => {
    api.get('subjects').then(response => {
      setSubjects(response.data);
    });
  }, []);

  function handleSupport(e: FormEvent) {
    e.preventDefault();

    api.post('tickets', { client: id, subject: subject }).then(response => {
      setTicket(response.data._id);
    });
  }

  return (
    <div id="page-register" className="container">
      <PageHeader title={`Bem vindo(a), ${clientName}.`} />
      { !ticket? (
      <form onSubmit={handleSupport} id="register-form">

        <Select 
          name="subject" 
          label="Assunto"
          value={subject}
          onChange={(e) => { setSubject(e.target.value); setSubjectName(e.target.options[e.target.selectedIndex].text) }}
          options={subjects}
        />

        <div className="buttons-container">
          <button type="submit" className="btn-register">
            Iniciar Atendimento
          </button>
          <Link to="/faq" className="btn-register">
            FAQ
          </Link>
        </div>
      </form>) : (<Chat label={`Assunto: ${subjectName}`} ticket={ticket} subject={subject} name="teste" />)}
    </div>
  );
}

export default Client;
