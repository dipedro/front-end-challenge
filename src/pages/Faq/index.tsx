import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { ExpandMore } from '@material-ui/icons';
import React, { useState, useEffect} from "react";
import PageHeader from "../../components/PageHeader";
import api from "../../services/api";
import "./styles.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(26),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

interface FaqProps {
  question: string,
  answer: string,
  _id: string
}

function Faq() {
  const classes = useStyles();
  const [faqs, setFaqs] = useState([]);

  useEffect(() => {
    api.get('faqs').then(response => {
      setFaqs(response.data);
    });

  }, []);

  return (
    <div id="page-register" className="container">
      <PageHeader title={`Perguntas Frequentes`} />
      {faqs.map( (faq: FaqProps) => {
       return (<Accordion key={faq._id}>
        <AccordionSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>{faq.question}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className={classes.heading}>
           Resposta: {faq.answer}
          </Typography>
        </AccordionDetails>
      </Accordion>)
      })}
      
    </div>
  );
}

export default Faq;
