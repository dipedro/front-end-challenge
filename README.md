# Frontend SAC Mateus

Frontend da aplicação SAC Mateus.

<p align="center">
  <img alt="Img SAC Mateus" src="https://i.imgur.com/7plCE48.png" width="100%">
  <img alt="Atendimento SAC Mateus" src="https://i.imgur.com/x5AoqkX.png" width="100%">
</p>

### Tecnologias
TypeScript, ReactJS e Socket.io

## Instalação

Clone o repositório, entre na pasta do projeto clonado e digite o seguinte comando, caso esteja utilizando o npm:

```bash
npm install
```

## Execução

Digite o seguinte comando, caso esteja utilizando o npm:

```bash
~ npm start

# Aguarde até aparecer a seguinte mensagem:
~ You can now view front-end in the browser.       

  Local:            http://localhost:3000 
```

## License
[MIT](https://choosealicense.com/licenses/mit/)